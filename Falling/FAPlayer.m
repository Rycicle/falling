//
//  FAPlayer.m
//  Falling
//
//  Created by Ryan Salton on 09/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "FAPlayer.h"
#import "FAEye.h"


@interface FAPlayer ()

@property (nonatomic, strong) SKShapeNode *leftEye;
@property (nonatomic, strong) SKShapeNode *rightEye;

@end

@implementation FAPlayer

- (instancetype)initWithLegLength:(CGFloat)legLength
{
    self = [super initWithImageNamed:@"body"];
    
    if (self)
    {
        self.zPosition = 1.0;
        
        self.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:20];
        self.physicsBody.dynamic = NO;
        
        for (NSInteger i = 0; i < 4; i++)
        {
            FALeg *leg = [[FALeg alloc] initWithPoint1:CGPointMake(0, 5 - (5 * i)) point2:CGPointMake(30, (-8 * i)) extendedPoint:CGPointMake(legLength, -8 * i) legSize:CGSizeMake(legLength, 20) withDelegate:i == 0? YES : NO];
            
            if (leg.delegateActive)
            {
                self.legWithDelegate = leg;
            }
            
            leg.position = CGPointMake(0, 0);
            [self addChild:leg];
            [self.legsArray addObject:leg];
        }
        
        for (NSInteger i = 0; i < 4; i++)
        {
            FALeg *leg = [[FALeg alloc] initWithPoint1:CGPointMake(0, 5 - (5 * i)) point2:CGPointMake(-30, (-8 * i)) extendedPoint:CGPointMake(-legLength, -8 * i) legSize:CGSizeMake(legLength, 20) withDelegate:NO];
            leg.position = CGPointMake(0, 0);
            [self addChild:leg];
            [self.legsArray addObject:leg];
        }
        
        self.leftEye.position = CGPointMake(-6.0, 0);
        self.rightEye.position = CGPointMake(6.0, 0);
        
    }
    
    return self;
}

- (SKShapeNode *)leftEye
{
    if (!_leftEye)
    {
        _leftEye = [[FAEye alloc] initWithEyeType:EyeTypeDefault];
        _leftEye.zPosition = 2.0;
        
        [self addChild:_leftEye];
        [self.eyesArray addObject:_leftEye];
    }
    
    return _leftEye;
}

- (SKShapeNode *)rightEye
{
    if (!_rightEye)
    {
        _rightEye = [[FAEye alloc] initWithEyeType:EyeTypeDefault];
        _rightEye.zPosition = 2.0;
        
        [self addChild:_rightEye];
        [self.eyesArray addObject:_rightEye];
    }
    
    return _rightEye;
}

- (NSMutableArray *)eyesArray
{
    if (!_eyesArray)
    {
        _eyesArray = [NSMutableArray array];
    }
    
    return _eyesArray;
}

- (NSMutableArray *)legsArray
{
    if (!_legsArray)
    {
        _legsArray = [NSMutableArray array];
    }
    
    return _legsArray;
}

@end
