//
//  FAWallNode.m
//  Falling
//
//  Created by Ryan Salton on 10/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "FAWallNode.h"
#import "FAWallSection.h"

@interface FAWallNode ()

@property (nonatomic, strong) NSMutableArray *leftWallArray;
@property (nonatomic, strong) NSMutableArray *rightWallArray;

@property (nonatomic, assign) CGFloat wallSectionHeight;

@end

@implementation FAWallNode

- (instancetype)initWithColor:(UIColor *)color size:(CGSize)size
{
    self = [super init];
    
    if (self)
    {
        self.wallSectionHeight = 30.0;
        
        [self createNextSectionAtY:0 startX:5];
    }
    
    return self;
}

- (void)createNextSectionAtY:(CGFloat)posY startX:(CGFloat)startX
{
    FAWallSection *section = [[FAWallSection alloc] initWithHeight:self.wallSectionHeight startX:startX maxX:10 minX:0 color:[UIColor blackColor] wallSideType:WallSideTypeLeft];
    section.position = CGPointMake(section.position.x, posY);
    [self.leftWallArray addObject:section];
    [self addChild:section];
    
    FAWallSection *rightSection = [[FAWallSection alloc] initWithHeight:self.wallSectionHeight startX:((FAWallSection *)self.rightWallArray.lastObject).endX maxX:10 minX:0 color:[UIColor blackColor] wallSideType:WallSideTypeRight];
    rightSection.position = CGPointMake(self.scene.size.width - rightSection.position.x, posY);
    [self.rightWallArray addObject:rightSection];
    [self addChild:rightSection];
}

- (void)moveWall:(CGFloat)moveY
{
    for (FAWallSection *section in self.leftWallArray)
    {
        section.position = CGPointMake(section.position.x, section.position.y + moveY);
    }
    
    for (FAWallSection *section in self.rightWallArray)
    {
        section.position = CGPointMake(section.position.x, section.position.y + moveY);
    }
}

- (void)checkWall
{
    FAWallSection *firstSection = self.leftWallArray.firstObject;
    FAWallSection *firstRightSection = self.rightWallArray.firstObject;
    FAWallSection *lastVisibleSection = self.leftWallArray.lastObject;
    
    if (self.position.y + lastVisibleSection.position.y > -1000)
    {
        [self createNextSectionAtY:(lastVisibleSection.position.y - (lastVisibleSection.frame.size.height)) startX:lastVisibleSection.endX];
    }
    
    if (self.position.y + firstSection.position.y > self.scene.size.height)
    {
        [firstSection removeFromParent];
        [firstRightSection removeFromParent];
        
        [self.leftWallArray removeObject:firstSection];
        [self.rightWallArray removeObject:self.rightWallArray.firstObject];
    }
}

- (NSMutableArray *)leftWallArray
{
    if (!_leftWallArray)
    {
        _leftWallArray = [NSMutableArray array];
    }
    
    return _leftWallArray;
}

- (NSMutableArray *)rightWallArray
{
    if (!_rightWallArray)
    {
        _rightWallArray = [NSMutableArray array];
    }
    
    return _rightWallArray;
}

@end
