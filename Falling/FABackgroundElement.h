//
//  FABackgroundElement.h
//  Falling
//
//  Created by Ryan Salton on 16/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef enum {
    ParentBackgroundTypeForest
} ParentBackgroundType;

typedef enum {
   ElementTypeSmall
} ElementType;

@interface FABackgroundElement : SKSpriteNode

- (instancetype)initWithType:(ParentBackgroundType)backgroundType;

@end
