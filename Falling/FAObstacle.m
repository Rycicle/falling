//
//  FAObstacle.m
//  Falling
//
//  Created by Ryan Salton on 11/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "FAObstacle.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@implementation FAObstacle

- (instancetype)initWithType:(ObstacleType)obstacleType maxX:(CGFloat)maxX
{
    self = [super initWithImageNamed:[FAObstacle imageNameForType:obstacleType]];
    
    if (self)
    {
        self.obstacleType = obstacleType;
        self.maxX = maxX;
        [self configureForType:obstacleType];
    }
    
    return self;
}

- (void)configureForType:(ObstacleType)type
{
    switch (type) {
        case ObstacleTypeCircularSawHorizontal:
            [self setScale:0.3];
            self.startX = 0;
            [self runAction:[SKAction repeatActionForever:[SKAction rotateByAngle:-M_PI duration:2.0]]];
            [self runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction moveToX:self.maxX duration:1.0], [SKAction waitForDuration:0.3], [SKAction moveToX:0 duration:1.0], [SKAction waitForDuration:0.3]]]]];
            break;
        case ObstacleTypeCircularSawHorizontal2:
            [self setScale:0.3];
            self.startX = self.maxX;
            [self runAction:[SKAction repeatActionForever:[SKAction rotateByAngle:-M_PI duration:2.0]]];
            [self runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction moveToX:0 duration:1.0], [SKAction waitForDuration:0.3], [SKAction moveToX:self.maxX duration:1.0], [SKAction waitForDuration:0.3]]]]];
            break;
        case ObstacleTypeVine:
            self.startX = 0;
            self.anchorPoint = CGPointMake(1.0, 0.5);
            [self runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction moveToX:self.maxX duration:0.5], [SKAction waitForDuration:0.3], [SKAction moveToX:50 duration:2.0], [SKAction waitForDuration:0.3]]]]];
            break;
        case ObstacleTypeVine2:
            self.startX = self.maxX;
            self.anchorPoint = CGPointMake(1.0, 0.5);
            [self runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction moveToX:0 duration:0.5], [SKAction waitForDuration:0.3], [SKAction moveToX:self.maxX - 50 duration:2.0], [SKAction waitForDuration:0.3]]]]];
            break;
        case ObstacleTypeCannon:
            [self setScale:0.6];
            self.startX = -10;
            self.anchorPoint = CGPointMake(0, 0.5);
            break;
        case ObstacleTypeCannon2:
            [self setScale:0.6];
            self.startX = self.maxX + 10;
            self.anchorPoint = CGPointMake(0, 0.5);
            break;
            
        default:
            break;
    }
}

- (void)addedToParent
{
    switch (self.obstacleType) {
        case ObstacleTypeVine2:
            self.xScale = -1;
            break;
//        case ObstacleTypeCannon2:
//            self.xScale = -1;
//            break;
            
        default:
            break;
    }
}

+ (NSString* )imageNameForType:(ObstacleType)type
{
    NSString *imageName = @"";
    
    switch (type) {
        case ObstacleTypeCircularSawHorizontal:
        case ObstacleTypeCircularSawHorizontal2:
            imageName = @"saw";
            break;
        case ObstacleTypeVine:
        case ObstacleTypeVine2:
            imageName = @"vine";
            break;
        case ObstacleTypeCannon:
        case ObstacleTypeCannon2:
            imageName = @"cannon";
            break;
        default:
            break;
    }
    
    return imageName;
}

@end
