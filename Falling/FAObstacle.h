//
//  FAObstacle.h
//  Falling
//
//  Created by Ryan Salton on 11/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef enum {
    ObstacleTypeCircularSawHorizontal,
    ObstacleTypeCircularSawHorizontal2,
    ObstacleTypeVine,
    ObstacleTypeVine2,
    ObstacleTypeCannon,
    ObstacleTypeCannon2,
    ObstacleTypeCount
} ObstacleType;

@interface FAObstacle : SKSpriteNode

@property (nonatomic, assign) ObstacleType obstacleType;
@property (nonatomic, assign) CGFloat maxX;
@property (nonatomic, assign) CGFloat startX;


- (instancetype)initWithType:(ObstacleType)obstacleType maxX:(CGFloat)maxX;
- (void)addedToParent;

@end
