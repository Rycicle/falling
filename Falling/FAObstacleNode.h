//
//  FAObstacleNode.h
//  Falling
//
//  Created by Ryan Salton on 11/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "FAObstacle.h"


@interface FAObstacleNode : SKNode

@property (nonatomic, strong) NSMutableArray *obstacles;
@property (nonatomic, assign) NSInteger obstacleFrequency;
@property (nonatomic, assign) CGSize size;

- (instancetype)initWithSize:(CGSize)size;
-(void)update:(CFTimeInterval)currentTime playerPosition:(CGPoint)playerPosition;
- (void)checkObstacles;
- (void)addObstacle;
- (void)moveObstacleNode:(CGFloat)moveY;

@end
