//
//  FAWallSection.h
//  Falling
//
//  Created by Ryan Salton on 10/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef enum {
    WallSideTypeLeft,
    WallSideTypeRight
} WallSideType;

@interface FAWallSection : SKShapeNode

@property (nonatomic, assign) CGFloat endX;
@property (nonatomic, assign) WallSideType wallSideType;

- (instancetype)initWithHeight:(CGFloat)height startX:(CGFloat)startX maxX:(CGFloat)maxX minX:(CGFloat)minX color:(UIColor *)color wallSideType:(WallSideType)wallSideType;

@end
