//
//  FALeg.m
//  Falling
//
//  Created by Ryan Salton on 09/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "FALeg.h"


@interface FALeg ()

@end

@implementation FALeg {
    SKShapeNode *leg;
}

- (instancetype)initWithPoint1:(CGPoint)point1 point2:(CGPoint)point2 extendedPoint:(CGPoint)extendedPoint legSize:(CGSize)size withDelegate:(BOOL)withDelegate
{
    self = [super initWithColor:[UIColor clearColor] size:size];
    
    if (self)
    {
        self.delegateActive = withDelegate;
        
        self.originPointNode.position = point1;
        self.endPointNode.position = point2;
        
        self.originPoint = point1;
        self.endPoint = point2;
        self.extendedPoint = extendedPoint;
        
        [self drawLeg];
    }
    
    return self;
}

- (void)drawLeg
{
    if (leg)
    {
        [leg removeFromParent];
    }
    CGFloat controlPointX = self.endPointNode.position.x * 0.5;
    CGFloat controlPointY = self.endPointNode.position.y + 30;
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, nil, self.originPoint.x, self.originPoint.y);
    CGPathAddQuadCurveToPoint(path, nil, controlPointX, controlPointY, self.endPointNode.position.x, self.endPointNode.position.y);
    
    leg = [SKShapeNode shapeNodeWithPath:path];
    leg.strokeColor = [UIColor blackColor];
    leg.lineWidth = 2.0;
    [self addChild:leg];
}

- (void)contract
{
    if (self.delegateActive)
    {
        [self.legDelegate legsAreExtended:NO];
    }
    
    [self.endPointNode runAction:[SKAction moveTo:self.endPoint duration:0.1]];
}

- (void)extend
{
    [self.endPointNode runAction:[SKAction moveTo:self.extendedPoint duration:0.1] completion:^{
        
        if (self.delegateActive && self.isTouching)
        {
            [self.legDelegate legsAreExtended:YES];
            [self.legDelegate throwDebris];
        }
        
    }];
}

- (SKSpriteNode *)originPointNode
{
    if (!_originPointNode)
    {
        _originPointNode = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(1, 1)];
        [self addChild:_originPointNode];
    }
    
    return _originPointNode;
}

- (SKSpriteNode *)endPointNode
{
    if (!_endPointNode)
    {
        _endPointNode = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(1, 1)];
        [self addChild:_endPointNode];
    }
    
    return _endPointNode;
}

@end
