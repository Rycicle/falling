//
//  FAWallNode.h
//  Falling
//
//  Created by Ryan Salton on 10/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface FAWallNode : SKNode

- (instancetype)initWithColor:(UIColor *)color size:(CGSize)size;
- (void)checkWall;
- (void)moveWall:(CGFloat)moveY;

@end
