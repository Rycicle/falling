//
//  FABackgroundNode.h
//  Falling
//
//  Created by Ryan Salton on 16/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef enum {
    BackgroundTypeForest
} BackgroundType;

@interface FABackgroundNode : SKNode

@property (nonatomic, assign) BackgroundType backgroundType;
@property (nonatomic, assign) CGFloat speed;

- (instancetype)initWithType:(BackgroundType)backgroundType speed:(CGFloat)speed;

@end
