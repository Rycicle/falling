//
//  FAEye.m
//  Falling
//
//  Created by Ryan Salton on 10/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "FAEye.h"

@implementation FAEye

- (instancetype)initWithEyeType:(EyeType)eyeType
{
    self = [FAEye shapeNodeWithCircleOfRadius:4.0];
    
    if (self)
    {
        self.fillColor = [UIColor whiteColor];
        
        SKShapeNode *pupil = [SKShapeNode shapeNodeWithCircleOfRadius:1.5];
        pupil.fillColor = [UIColor blackColor];
        [self addChild:pupil];
    }
    
    return self;
}

- (void)blink
{
    [self runAction:[SKAction scaleYTo:0 duration:0.05] completion:^{
        [self runAction:[SKAction scaleYTo:1.0 duration:0.05]];
    }];
}

@end
