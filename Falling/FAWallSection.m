//
//  FAWallSection.m
//  Falling
//
//  Created by Ryan Salton on 10/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "FAWallSection.h"

@implementation FAWallSection

- (instancetype)initWithHeight:(CGFloat)height startX:(CGFloat)startX maxX:(CGFloat)maxX minX:(CGFloat)minX color:(UIColor *)color wallSideType:(WallSideType)wallSideType
{
    self = [super init];
    
    if (self)
    {
        self.wallSideType = wallSideType;
        
        CGMutablePathRef path = [self generateSectionWithStartX:startX maxX:maxX minX:minX height:height];
        self.path = path;
        CGPathRelease(path);
        
        self.fillColor = color;
        self.strokeColor = [UIColor clearColor];
    }
    
    return self;
}

- (CGMutablePathRef)generateSectionWithStartX:(CGFloat)startX maxX:(CGFloat)maxX minX:(CGFloat)minX height:(CGFloat)height
{
    
    self.endX = minX + arc4random_uniform(maxX - minX);
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    if (self.wallSideType == WallSideTypeLeft){
        CGPathMoveToPoint(path, nil, 0, 0);
        CGPathAddLineToPoint(path, nil, self.endX, 0);
        CGPathAddLineToPoint(path, nil, minX + arc4random_uniform(maxX - minX), arc4random_uniform(height));
        CGPathAddLineToPoint(path, nil,  startX, height);
        CGPathAddLineToPoint(path, nil, 0, height);
    }
    else
    {
        CGPathMoveToPoint(path, nil, 0, 0);
        CGPathAddLineToPoint(path, nil, -self.endX, 0);
        CGPathAddLineToPoint(path, nil, -(minX + arc4random_uniform(maxX - minX)), arc4random_uniform(height));
        CGPathAddLineToPoint(path, nil, -startX, height);
        CGPathAddLineToPoint(path, nil, 0, height);
    }
    
//    CGPathRelease(path);
    
    
    return path;
    
}

@end
