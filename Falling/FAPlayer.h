//
//  FAPlayer.h
//  Falling
//
//  Created by Ryan Salton on 09/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "FALeg.h"


@interface FAPlayer : SKSpriteNode

@property (nonatomic, strong) NSMutableArray *eyesArray;
@property (nonatomic, strong) NSMutableArray *legsArray;

@property (nonatomic, strong) FALeg *legWithDelegate;

- (instancetype)initWithLegLength:(CGFloat)legLength;

@end
