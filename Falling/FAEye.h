//
//  FAEye.h
//  Falling
//
//  Created by Ryan Salton on 10/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef enum {
    EyeTypeDefault
} EyeType;

@interface FAEye : SKShapeNode

@property (nonatomic, assign) EyeType eyeType;

- (instancetype)initWithEyeType:(EyeType)eyeType;
- (void)blink;

@end
