//
//  FAObstacleNode.m
//  Falling
//
//  Created by Ryan Salton on 11/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "FAObstacleNode.h"

@implementation FAObstacleNode {
    NSInteger frequencyTimer;
}

- (instancetype)initWithSize:(CGSize)size
{
    self = [super init];
    
    if (self)
    {
        self.size = size;
        self.obstacleFrequency = 200;
        frequencyTimer = 0;
    }
    
    return self;
}

- (void)checkObstacles
{
    if (frequencyTimer >= self.obstacleFrequency)
    {
        [self addObstacle];
        frequencyTimer = 0;
    }
    
    NSMutableArray *objectsToRemove = [NSMutableArray array];
    
    for (FAObstacle *obstacle in self.obstacles)
    {
        if (self.position.y + (obstacle.position.y - (obstacle.size.height * 0.5)) > self.size.height + 100)
        {
            [objectsToRemove addObject:obstacle];
            [obstacle removeFromParent];
        }
    }
    
    [self.obstacles removeObjectsInArray:objectsToRemove];
}

- (void)moveObstacleNode:(CGFloat)moveY
{
    frequencyTimer += moveY;
    self.position = CGPointMake(self.position.x, self.position.y + moveY);
}

- (void)addObstacle
{
    FAObstacle *obstacle = [[FAObstacle alloc] initWithType:arc4random_uniform(ObstacleTypeCount) maxX:self.size.width];
//    FAObstacle *obstacle = [[FAObstacle alloc] initWithType:ObstacleTypeCannon2 maxX:self.size.width];
    obstacle.position = CGPointMake(obstacle.startX, - (self.position.y + 200));
    [self addChild:obstacle];
    [self.obstacles addObject:obstacle];
}

- (void)addChild:(SKNode *)node
{
    [super addChild:node];
    
    if ([node isKindOfClass:[FAObstacle class]])
    {
        [(FAObstacle *)node addedToParent];
    }
}

- (void)update:(CFTimeInterval)currentTime playerPosition:(CGPoint)playerPosition
{
    for (FAObstacle *obstacle in self.obstacles)
    {
        if (obstacle.obstacleType == ObstacleTypeCannon || obstacle.obstacleType == ObstacleTypeCannon2)
        {
            CGFloat diffY = (self.position.y + obstacle.position.y) - playerPosition.y;
            CGFloat angleDegrees = atan2f(diffY, (playerPosition.x - obstacle.position.x));
            
            [obstacle runAction:[SKAction rotateToAngle:-angleDegrees duration:0.0]];
        }
    }
}

- (NSMutableArray *)obstacles
{
    if (!_obstacles)
    {
        _obstacles = [NSMutableArray array];
    }
    
    return _obstacles;
}

@end
