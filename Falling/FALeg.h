//
//  FALeg.h
//  Falling
//
//  Created by Ryan Salton on 09/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@protocol LegDelegate;

@interface FALeg : SKSpriteNode

@property (nonatomic, weak) id<LegDelegate> legDelegate;

@property (nonatomic, assign) CGPoint originPoint;
@property (nonatomic, assign) CGPoint endPoint;
@property (nonatomic, assign) CGPoint extendedPoint;

@property (nonatomic, strong) SKSpriteNode *originPointNode;
@property (nonatomic, strong) SKSpriteNode *endPointNode;

@property (nonatomic, assign) BOOL delegateActive;
@property (nonatomic, assign) BOOL isTouching;

- (instancetype)initWithPoint1:(CGPoint)point1 point2:(CGPoint)point2 extendedPoint:(CGPoint)extendedPoint legSize:(CGSize)size withDelegate:(BOOL)withDelegate;
- (void)extend;
- (void)contract;
- (void)drawLeg;

@end

@protocol LegDelegate <NSObject>

- (void)legsAreExtended:(BOOL)extended;
- (void)throwDebris;

@end
