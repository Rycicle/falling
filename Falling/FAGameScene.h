//
//  FAGameScene.h
//  Falling
//
//  Created by Ryan Salton on 09/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "FALeg.h"


@interface FAGameScene : SKScene <LegDelegate>

@end
