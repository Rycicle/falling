//
//  FAGameScene.m
//  Falling
//
//  Created by Ryan Salton on 09/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "FAGameScene.h"
#import "FAPlayer.h"
#import "FAEye.h"
#import "FAWallNode.h"
#import "FADebris.h"
#import "FAObstacleNode.h"
#import "FABackgroundNode.h"


static const uint32_t noneCategory = 0x1 << 1;
static const uint32_t spiderCategory = 0x1 << 2;
static const uint32_t debrisCategory = 0x1 << 3;
//static const uint32_t webCategory = 0x1 << 4;

@interface FAGameScene ()

@property (nonatomic, strong) FAPlayer *player;
@property (nonatomic, strong) FAWallNode *wallNode;
@property (nonatomic, strong) FAObstacleNode *obstacleNode;

@property (nonatomic, strong) FABackgroundNode *background;

@property (nonatomic, assign) NSInteger blinkTimer;
@property (nonatomic, assign) CGFloat currentSpeed;
@property (nonatomic, assign) BOOL isHolding;

@property (nonatomic, strong) NSMutableArray *debrisArray;

@property (nonatomic, strong) SKAction *impactNoise;

//@property (nonatomic, strong) NSMutableArray *webSections;

@end


@implementation FAGameScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    self.blinkTimer = 0;
    
    self.physicsWorld.gravity = CGVectorMake(0, -3);
    
    self.backgroundColor = [UIColor colorWithRed:0.205 green:0.344 blue:0.592 alpha:1.000];
    
    self.background.position = CGPointMake(0, self.size.height);
    
    self.player.position = CGPointMake(self.size.width * 0.5, self.size.height * 0.5);
    
    self.obstacleNode.position = CGPointMake(0, 0);
    
    self.wallNode.position = CGPointMake(0, self.size.height);
    
    self.impactNoise = [SKAction playSoundFileNamed:@"impact.mp3" waitForCompletion:NO];
}

- (void)legsAreExtended:(BOOL)extended
{
    self.isHolding = extended;
}

- (void)throwDebris
{
    [self runAction:self.impactNoise];
    
    [self.debrisArray removeAllObjects];
    
    for (NSInteger i = 0; i < 4; i++)
    {
        FADebris *debris = [FADebris shapeNodeWithCircleOfRadius:1 + arc4random_uniform(2)];
        debris.position = CGPointMake(0, self.player.position.y);
        [self.debrisArray addObject:debris];
        [self addChild:debris];
        
        [debris.physicsBody applyImpulse:CGVectorMake(arc4random_uniform(2), arc4random_uniform(3))];
    }
    
    for (NSInteger k = 0; k < 4; k++)
    {
        NSInteger randomNumber = arc4random_uniform(2);
        
        FADebris *debris = [FADebris shapeNodeWithCircleOfRadius:1 + arc4random_uniform(2)];
        debris.position = CGPointMake(self.size.width - 5, self.player.position.y);
        [self.debrisArray addObject:debris];
        [self addChild:debris];
        
        [debris.physicsBody applyImpulse:CGVectorMake(randomNumber * -1, arc4random_uniform(3))];
    }
    
    for (FADebris *debris in self.debrisArray)
    {
        [debris runAction:[SKAction sequence:@[[SKAction waitForDuration:1.0], [SKAction fadeAlphaTo:0.0 duration:1.0], [SKAction removeFromParent]]]];
    }
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    for (FALeg *leg in self.player.legsArray)
    {
        leg.isTouching = YES;
        [leg extend];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    for (FALeg *leg in self.player.legsArray)
    {
        leg.isTouching = NO;
        [leg contract];
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    self.blinkTimer++;
    
    if (self.blinkTimer >= 200)
    {
        self.blinkTimer = 0;
        for(FAEye *eye in self.player.eyesArray)
        {
            [eye blink];
        }
    }
    
    for (FALeg *leg in self.player.legsArray)
    {
        [leg drawLeg];
    }
    
    if (self.isHolding)
    {
        self.currentSpeed = 0.5;
        self.player.position = CGPointMake(self.player.position.x, self.player.position.y + self.currentSpeed);
    }
    else
    {
        self.currentSpeed += 0.05;
        
        if (self.currentSpeed > 10)
        {
            self.currentSpeed = 10;
        }
        
        if (self.player.position.y > self.size.height * 0.5)
        {
            self.player.position = CGPointMake(self.player.position.x, self.player.position.y - 0.2);
        }
    }
    
    [self.wallNode moveWall:self.currentSpeed];
    
    [self.wallNode checkWall];
    
    [self.obstacleNode moveObstacleNode:self.currentSpeed];
    
    [self.obstacleNode checkObstacles];
    
    [self.obstacleNode update:currentTime playerPosition:self.player.position];
}

- (FAPlayer *)player
{
    if (!_player)
    {
        _player = [[FAPlayer alloc] initWithLegLength:self.size.width * 0.5];
        [self addChild:_player];
        
        _player.legWithDelegate.legDelegate = self;
    }
    
    return _player;
}

- (FAWallNode *)wallNode
{
    if (!_wallNode)
    {
        _wallNode = [[FAWallNode alloc] initWithColor:[UIColor blackColor] size:self.size];
        [self addChild:_wallNode];
    }
    
    return _wallNode;
}

- (FAObstacleNode *)obstacleNode
{
    if (!_obstacleNode)
    {
        _obstacleNode = [[FAObstacleNode alloc] initWithSize:self.size];
        [self addChild:_obstacleNode];
    }
    
    return _obstacleNode;
}

- (FABackgroundNode *)background
{
    if (!_background)
    {
        _background = [[FABackgroundNode alloc] initWithType:BackgroundTypeForest speed:1.0];
    }
    
    return _background;
}

- (NSMutableArray *)debrisArray
{
    if (!_debrisArray)
    {
        _debrisArray = [NSMutableArray array];
    }
    
    return _debrisArray;
}

@end
