//
//  FADebris.m
//  Falling
//
//  Created by Ryan Salton on 11/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "FADebris.h"

@implementation FADebris

+ (instancetype)shapeNodeWithCircleOfRadius:(CGFloat)radius
{
    FADebris *debris = [super shapeNodeWithCircleOfRadius:radius];
    debris.fillColor = [UIColor blackColor];
    debris.strokeColor = [UIColor blackColor];
    debris.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(10, 10)];
    
    return debris;
}

@end
